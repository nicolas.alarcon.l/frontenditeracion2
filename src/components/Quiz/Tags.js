import React, { Component } from 'react';
import cx from 'classnames';
import axios from 'axios';
import ReactDOM from 'react-dom';
import urlBase from '../../routes';

class Tags extends Component {



   constructor(props){
    super(props);
      this.state = {
          

    
    
    }
  }

  componentDidMount(){
    //fetch de todos los quizes. se almacenan en state.quizzes
    axios.get(urlBase+'/quizzes')
        .then(res => {
          console.log(res);
          this.setState({ quizzes: res.data });
        });

    //por ahora se predefine el quiz de ejemplo con id 143.
    //la idea, mas adelante, es que se seleccione un quizz de la lista en base a los datos de cursos y usuario
    axios.get(urlBase+'/quiz/143/materias')
        .then(res => {
          console.log(res.data);
          this.setState({materias: res.data});
          //con esta funcion se agregan los codigos con sus respuestas a las listas respectivas
          this.fetchPreguntas();
        });
  }




  async fetchPreguntas() {
    var x;
    var _preguntas = [];
    var _respuestasCorrectas = [];
    for(x in this.state.materias) {
      var auxPregunta = "";
      var auxRespuesta = "";
      var materia = this.state.materias[x];
      var materia_id = materia.materia_id;
      console.log(materia);
      console.log(materia_id);
      //console.log(materia);
      let json = await axios.get('http://206.189.198.100:8081/backend-0.0.1-SNAPSHOT/app/obtener_pregunta_' + 131)
          .then(res => {
            auxPregunta = res.data.pregunta;
            auxRespuesta = res.data.respuesta;
            auxRespuesta = auxRespuesta.replace(/(\r\n\t|\n|\r\t)/gm,"");
      //console.log(res); 
            //(res.data.pregunta);
            //(res.data.respuesta);
            //console.log(auxPregunta);
            //console.log(auxRespuesta);
            _preguntas.push(auxPregunta);
            _respuestasCorrectas.push(auxRespuesta);
          });
      this.setState({preguntas: _preguntas});
      this.setState({respuestasCorrectas: _respuestasCorrectas});
    }
  }

  handleInputChanged(e){
    //console.log( e.target.value);
    if(e.target.name === "respuesta_1"){
      console.log("respuesta1");
      this.setState({
        respuesta1: e.target.value.toString()
      });
    }else if (e.target.name === "respuesta_2"){
          console.log("respuesta2");
      this.setState({
        respuesta2: e.target.value.toString()
      });
    }else if (e.target.name === "respuesta_3"){
      this.setState({
        respuesta3: e.target.value.toString()
      });
    }
  }




  render() {
    return (
       <div className="row">
    <div className="col-md-12">
      <div className="card">
        <div className="header">
          <h4 className="title">a Nº1</h4>
         
        </div>
        <div className="content">
          <Tabs defaultActiveKey={1} id="plan-text-tabs">
            <Tab eventKey={1} title="Pregunta Nº 1">

             <h6> PREGUNTA  Nº1 </h6>


             <h6> {this.state.preguntas[0]} </h6>


                  <div className="col-md-12">
                      <FormElements initialValues={{
                        radioGroup: 'male',
                        a: true,
                        checked: true,
                        disabledChecked: true,
                        radioOnOff: 'on',
                        radioDisabledOnOff: 'on'
                      }} />
                  </div>

                 <center>  <button type="button" className="btn btn-wd btn-success">
                  <span className="btn-label">
                  <i className="fa fa-check"></i>
                  </span>  Enviar 
                  </button> </center>





            </Tab>
            <Tab eventKey={2} title="Pregunta Nº 2">

                <h6> PREGUNTA  Nº2 </h6>
             <h6> VER FOMRA DE VISUALIZAR COMO IMAGEN </h6>


                  <div className="col-md-12">
                      <FormElements initialValues={{
                        radioGroup: 'male',
                        a: true,
                        checked: true,
                        disabledChecked: true,
                        radioOnOff: 'on',
                        radioDisabledOnOff: 'on'
                      }} />
                  </div>


                 <center>  <button type="button" className="btn btn-wd btn-success">
                  <span className="btn-label">
                  <i className="fa fa-check"></i>
                  </span>  Enviar 
                  </button> </center>


            </Tab>
            <Tab eventKey={3} title="Pregunta Nº 3">
                  <h6> PREGUNTA  Nº3 </h6>
             <h6> VER FOMRA DE VISUALIZAR COMO IMAGEN </h6>


                  <div className="col-md-12">
                      <FormElements initialValues={{
                        radioGroup: 'male',
                        a: true,
                        checked: true,
                        disabledChecked: true,
                        radioOnOff: 'on',
                        radioDisabledOnOff: 'on'
                      }} />
                  </div>


                 <center>  <button type="button" className="btn btn-wd btn-success">
                  <span className="btn-label">
                  <i className="fa fa-check"></i>
                  </span>  Enviar 
                  </button> </center>



            </Tab>
            

          </Tabs>
        </div>
      </div>
    </div>
  </div>
  );
}

}

export default Tags;