import React from 'react';
import generateData from './generateData';
import Quizzes from './quizzes';

const data = generateData(5);

const Dashboard = () => (
  <div className="content">
    <div className="container-fluid">
      <div className="row">
        <div className="col-md-12">
               <Quizzes />
        </div>
      </div>
    </div>
  </div>
);

export default Dashboard;
