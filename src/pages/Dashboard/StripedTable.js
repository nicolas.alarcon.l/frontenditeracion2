import React from 'react';
import { Link } from 'react-router-dom';


const StripedTable = ({data}) => (
  <div className="card">
    <div className="header">
      <h4 className="title">Notas </h4>
      <p className="category">Lista de notas</p>
    </div>
    <div className="content table-responsive table-full-width">
      <table className="table table-hover table-striped">
        <thead>
          

          <tr>
            <th> QUIZ </th>
            <th> UNIDADES </th>
            <th>FECHA </th>
            <th> NOTA </th>
             <th> VER RESPUESTAS </th>
          </tr>


        </thead>
        <tbody>
          {data.map(item => (
            <tr key={item.id}>
              <td>{item.id}</td>
              <td> </td>
              <td> </td>
              <td> </td>
              
              <td> 

                 <Link to="/components/grid">
                          <button type="button" className="btn btn-wd btn-warning">
                <span className="btn-label">
                </span> Ver Detalles 
              </button>

               </Link>


           </td>
            </tr>
          ))}
        </tbody>
      </table>

    </div>
  </div>
);

export default StripedTable;