
import React, { Component } from 'react';
import axios from 'axios';
import { Accordion, Panel } from 'react-bootstrap';
import { Tabs, Tab } from 'react-bootstrap';
import urlBase from '../../routes';
import AceEditor from 'react-ace';
import SweetAlert from 'sweetalert-react';
import 'brace/theme/eclipse';
import Loader from 'react-loader-spinner';
import Countdown from 'react-countdown-now';



class Quizzes extends Component {

	 constructor(props){
    super(props);
      this.state = {

      quizzes: [],
      comenzarQuiz: false,
      preguntas: [],
      respuestasCorrectas: [],
      puntajes: [],
      respuesta1: " ",
      respuesta2: " ",
      respuesta3:  "",
      puntaje1: 0,
      puntaje2: 0,
      puntaje3: 0,
      suma:0,
      nota:0,
      historialQuiz:[],
      quizActual: [],
      quizActualId: 0,
      quizFinalizado: false,
      mounted: false,
      show: false,
      show2:false,
      show3:false,
      show4:false,
      cargandoQuiz:false,
      minutosDuracion: 0,
      tiempoFuera: false,
      dateNow: [],
      notaEsRoja: false,
      showAlertTimeout: false
    }
    this.disableBotones = this.disableBotones.bind(this);
    this.comenzarQuiz = this.comenzarQuiz.bind(this);
    this.handleInputChanged = this.handleInputChanged.bind(this);
    this.enviarRespuesta1 = this.enviarRespuesta1.bind(this);
    this.enviarRespuesta2 = this.enviarRespuesta2.bind(this);
    this.enviarRespuesta3 = this.enviarRespuesta3.bind(this);
    this.terminarQuiz = this.terminarQuiz.bind(this);
    this.volverAlListado = this.volverAlListado.bind(this);
  }
 componentDidMount(){
  //fetch de todos los quizes. se almacenan en state.quizzes
  axios.get(urlBase+'/quizzes')
   .then(res => {
    //console.log(res);
     this.setState({ quizzes: res.data });
  });
  }

  handleInputChanged(e){
    console.log( e.target.value);
    if(e.target.name === "respuesta1"){
      this.setState({
        respuesta1: e.target.value.toString()
      });
    }else if (e.target.name === "respuesta2"){
      this.setState({
        respuesta2: e.target.value.toString()
      });
    }else if (e.target.name === "respuesta3"){
      this.setState({
        respuesta3: e.target.value.toString()
      });
    }
  }

  enviarRespuesta1() {
    var estado = {
        respuestaAlumno: this.state.respuesta1,
        numeroRespuesta: 1
    };
    var respuestaId = this.state.historialQuiz.respuestaAlmacenadaList[0].respuestaAlmacenadaId;
    axios.put(urlBase+'/respuestas/update/'+respuestaId, estado)
    .then(res => {
        if(res.data.putajeRespuesta == 2) {
          this.setState({puntaje1: this.state.puntajes[0]});
          axios.put(urlBase+'/respuestas/update/'+respuestaId, {respuestaAlumno: this.state.respuesta1, numeroRespuesta:1, putajeRespuesta: this.state.puntaje1});
        }
        this.setState({show:true})})
        .catch(error =>{
            alert(error)
    })
  }

  enviarRespuesta2() {
    var estado = {
        respuestaAlumno: this.state.respuesta2,
        numeroRespuesta: 2
    };
    var respuestaId = this.state.historialQuiz.respuestaAlmacenadaList[1].respuestaAlmacenadaId;
    axios.put(urlBase+'/respuestas/update/'+respuestaId, estado)
        .then(res => {
            if(res.data.putajeRespuesta == 2) {
              this.setState({puntaje2: this.state.puntajes[1]});
              axios.put(urlBase+'/respuestas/update/'+respuestaId, {respuestaAlumno: this.state.respuesta2, numeroRespuesta:2, putajeRespuesta: this.state.puntaje2})
            }
            this.setState({show2: true})})
            .catch(error =>{
                alert(error)
        })
  }

disableBotones(){
  console.log("hola")
  this.setState({tiempoFuera: true});
  this.setState({showAlertTimeout: true});
}

  enviarRespuesta3() {
      var estado = {
          respuestaAlumno: this.state.respuesta3,
          numeroRespuesta: 3
      };
      var respuestaId = this.state.historialQuiz.respuestaAlmacenadaList[2].respuestaAlmacenadaId;
      axios.put(urlBase+'/respuestas/update/'+respuestaId, estado)
          .then(res => {
              if(res.data.putajeRespuesta == 2) {
                this.setState({puntaje3: this.state.puntajes[2]});
                axios.put(urlBase+'/respuestas/update/'+respuestaId, {respuestaAlumno: this.state.respuesta3, numeroRespuesta:3, putajeRespuesta: this.state.puntaje3})
              }
              this.setState({show3:true})})
              .catch(error =>{
                  alert(error)
          })
  }

  terminarQuiz(){
    axios.get(urlBase+'/historial/'+this.state.historialQuiz.historialId)
    .then(res => {
      this.setState({historialQuiz: res.data});
      this.calcularNota();
			this.setState({show4: true})})
			.catch(error =>{
					alert(error)
    }).finally(this.setState({quizFinalizado: true}));
  }

  calcularNota() {
    var total = this.state.puntajes.reduce((a,b) => a+b, 0);
    var obtenido = this.state.puntaje1+this.state.puntaje2+this.state.puntaje3;
    var porcentaje = obtenido/total;
    var notaAux = 6.0*porcentaje + 1.0;
    notaAux = +notaAux.toFixed(2);
    if(notaAux < 4.0) {
      this.setState({notaEsRoja: true});
    }
    this.setState({suma: obtenido});
    this.setState({nota: notaAux});
  }

  volverAlListado() {
    this.setState({quizFinalizado: false});
    axios.get(urlBase+'/habilitar_deshabilitar_quizz/'+this.state.quizActual.quizId)
    .then(res => {
      axios.get(urlBase+'/quizzes')
      .then(res2 => {
        this.setState({quizzes : res2.data})
      })
    }).finally( this.setState({comenzarQuiz: false}))
  }

  buscarQuiz(llave, quizzes) {
    for(var i = 0; i<quizzes.length; i++) {
      if(quizzes[i].quizId == llave) {
        return quizzes[i];
      }
    }
  }

  async comenzarQuiz(e) {
    this.setState({dateNow: Date.now()});
    this.setState({cargandoQuiz: true});//para renderizar el spinload
    e.persist();
    let json = await axios.post(urlBase+'/historial/create',{"quizId": e.target.id});
    this.setState({historialQuiz:json.data});
    this.setState({mounted:true});
 
      var historialQuizId = this.state.historialQuiz.historialId;
      var thisQuiz = this.buscarQuiz(e.target.id, this.state.quizzes);
      this.setState({quizActual: thisQuiz});
      this.setState({minutosDuracion: thisQuiz.minutosDuracion});
      var x;
      var _preguntas = [];
      var _respuestasCorrectas = [];
      var _puntajes = [];
      for(x in thisQuiz.quizMateriaList) {
        var auxPregunta = "";
        var auxRespuesta = "";
        var materia = await axios.get(urlBase+'/materia/quiz/'+thisQuiz.quizMateriaList[x].id);//thisQuiz.materias[x];
        var materia = materia.data;
        _puntajes.push(materia.quizMateriaList[0].puntajePregunta);
        var materiaId = materia.materiaId;
        let json = await axios.get(urlBase+'/obtener_pregunta_' + materiaId + '/' + historialQuizId);
        console.log(json.data);
        auxPregunta = json.data;

        _preguntas.push(auxPregunta);
        _respuestasCorrectas.push(auxRespuesta);
        this.setState({preguntas: _preguntas});
        this.setState({respuestasCorrectas: _respuestasCorrectas});
        this.setState({puntajes: _puntajes});
      }

      axios.get(urlBase+'/historial/'+this.state.historialQuiz.historialId)
        .then(res => {
            this.setState({historialQuiz: res.data});
        });
    this.setState({comenzarQuiz: true});
    this.setState({cargandoQuiz: false});//dejar de renderizar spinload
  }

  render() {
    var self = this;
    if(this.state.comenzarQuiz) {
      return (
        <div className="row">
		     <div className="col-md-12">
		      <div className="card">
		      	<div className="header" >          
		        	<h4 className="title">Quiz: {this.state.quizActual.nombreQuiz}</h4>
         		</div>
						<div className="content">
            <SweetAlert
										          show={this.state.showAlertTimeout}
										          title="Se acabó el tiempo"
										          text="Debes enviar tu quiz ahora"
										          onConfirm={() => this.setState({ showAlertTimeout: false })}
													  />
					 		<Tabs defaultActiveKey={1} id="plan-text-tabs">
						 		<Tab eventKey={1} title="Pregunta Nº 1">
									<table className="table table-bordered">
										<thead className = "thead">
											<tr>
				              	<th scope = "col">Pregunta</th>
				                <th scope = "col">Respuesta</th>
				                <th scope = "col">Enviar Respuesta</th>
				              </tr>
										</thead>
				            <tbody>
											<tr>
						      			<td>
													<AceEditor
														 mode="python"
															name="blah2"
															theme="eclipse"
															height="200px"
															width="500px"
														 onLoad={this.onLoad}
															onChange={this.onChange}
															readOnly={true}
														 fontSize={14}
														 showPrintMargin={true}
														 showGutter={true}
														 highlightActiveLine={true}
															value={this.state.preguntas[0]}
															defaultValue={this.state.preguntas[0]}
														/>
													<br/>
					              </td>
					              <td>
					                <input type="text" name="respuesta1" onChange={this.handleInputChanged} />
					              </td>
					              <td>
                          {this.state.tiempoFuera ? (<button type="button" onClick={this.enviarRespuesta1} className="btn btn-wd btn-success" disabled > <span className="btn-label">
				                      <i className="fa fa-check"></i>
				                    </span>  Enviar</button>):( <button type="button" onClick={this.enviarRespuesta1} className="btn btn-wd btn-success">
														<SweetAlert
										          show={this.state.show}
										          title="Guardada"
										          text="Respuesta 1 guardada"
										          onConfirm={() => this.setState({ show: false })}
													  />
				                    <span className="btn-label">
				                      <i className="fa fa-check"></i>
				                    </span>  Enviar
					                 </button>)}
					               
					               </td>
				              </tr>
										</tbody>
									</table>
								</Tab>
								<Tab eventKey={2} title="Pregunta Nº 2">
									<table className="table table-bordered">
										<thead className = "thead">
											<tr>
				              	<th scope = "col">Pregunta</th>
				                <th scope = "col">Respuesta</th>
				                <th scope = "col">Enviar Respuesta</th>
				              </tr>
										</thead>
				            <tbody>
											<tr>
						      			<td>
												<AceEditor
														mode="python"
														 name="blah3"
														 theme="eclipse"
														 height="200px"
														 width="500px"
														onLoad={this.onLoad}
														 onChange={this.onChange}
														 readOnly={true}
														fontSize={14}
														showPrintMargin={true}
														showGutter={true}
														highlightActiveLine={true}
														 value={this.state.preguntas[1]}
														 defaultValue={this.state.preguntas[1]}
														/>
													<br/>
					              </td>
					              <td>
					                <input type="text" name="respuesta2" onChange={this.handleInputChanged} />
					              </td>
					              <td>
                          {this.state.tiempoFuera ? (<button type="button" onClick={this.enviarRespuesta2} className="btn btn-wd btn-success" disabled > <span className="btn-label">
				                      <i className="fa fa-check"></i>
				                    </span>  Enviar</button>):( <button type="button" onClick={this.enviarRespuesta2} className="btn btn-wd btn-success">
														<SweetAlert
										          show={this.state.show2}
										          title="Guardada"
										          text="Respuesta 2 guardada"
										          onConfirm={() => this.setState({ show2: false })}
													  />
				                    <span className="btn-label">
				                      <i className="fa fa-check"></i>
				                    </span>  Enviar
					                 </button>)}
					               
					               </td>
				              </tr>
										</tbody>
									</table>
								</Tab>
								<Tab eventKey={3} title="Pregunta Nº 3">
									<table className="table table-bordered">
										<thead className = "thead">
											<tr>
				              	<th scope = "col">Pregunta</th>
				                <th scope = "col">Respuesta</th>
				                <th scope = "col">Enviar Respuesta</th>
				              </tr>
										</thead>
				            <tbody>
											<tr>
						      			<td>
												<AceEditor
 			                      mode="python"
                             name="blah4"
                             theme="eclipse"
                             height="200px"
                             width="500px"
 			                      onLoad={this.onLoad}
                             onChange={this.onChange}
                             readOnly={true}
 			                      fontSize={14}
 			                      showPrintMargin={true}
 			                      showGutter={true}
 			                      highlightActiveLine={true}
                             value={this.state.preguntas[2]}
                             defaultValue={this.state.preguntas[2]}
 			                      />
													<br/>
					              </td>
					              <td>
					                <input type="text" name="respuesta3" onChange={this.handleInputChanged} />
					              </td>
					              <td>
                          {this.state.tiempoFuera ? (<button type="button" onClick={this.enviarRespuesta3} className="btn btn-wd btn-success" disabled > <span className="btn-label">
				                      <i className="fa fa-check"></i>
				                    </span>  Enviar</button>):( <button type="button" onClick={this.enviarRespuesta3} className="btn btn-wd btn-success">
														<SweetAlert
										          show={this.state.show3}
										          title="Guardada"
										          text="Respuesta 3 guardada"
										          onConfirm={() => this.setState({ show3: false })}
													  />
				                    <span className="btn-label">
				                      <i className="fa fa-check"></i>
				                    </span>  Enviar
					                 </button>)}
					               
					               </td>
				              </tr>
										</tbody>
									</table>
									<br/>
									<center>
										<button type="button" onClick={this.terminarQuiz} className="btn btn-wd btn-success">
											<SweetAlert
												 show={this.state.show4}
												 title="Finalizado"
												 text="Quiz Terminado"
												 onConfirm={() => this.setState({ show4: false })}
											 />
										 <span className="btn-label">
										 	<i className="fa fa-check"></i>
										 </span>  Terminar Quiz
										 </button>
										</center>
								</Tab>
             {this.state.quizFinalizado ? (<Tab eventKey={4} title="Resultados quiz">
                   <h6> RESULTADOS </h6>
                   <div className="card">
                     <div className="header">
                       <h4 className="title">Información quiz rendido </h4>
                       <p className="category">Detalles</p>
                     </div>
                     <div className="content table-responsive table-full-width">
                       <table className="table table-hover table-striped">
                         <thead>
                           <tr>
                             <th> Puntaje respuesta 1 </th>
                             <th> Puntaje respuesta 2 </th>
                             <th> Puntaje respuesta 3 </th>
                             <th> Nota obtenida </th>
                           </tr>
                         </thead>
                         <tbody>
                             <tr>
                               <td>{this.state.historialQuiz.respuestaAlmacenadaList[0].putajeRespuesta} de {this.state.puntajes[0]}</td>
                               <td>{this.state.historialQuiz.respuestaAlmacenadaList[1].putajeRespuesta} de {this.state.puntajes[1]}</td>
                               <td>{this.state.historialQuiz.respuestaAlmacenadaList[2].putajeRespuesta} de {this.state.puntajes[2]}</td>
                               {this.state.notaEsRoja ? (<td><p style={{color: "red"}}><big><strong>{this.state.nota}</strong></big></p></td>) : (<td><p style={{color: "blue"}}><big><strong>{this.state.nota}</strong></big></p></td>)}
                             </tr>
                         </tbody>
                         <thead>
                           <tr>
                             <th> Respuesta correcta 1 </th>
                             <th> Respuesta correcta 2 </th>
                             <th> Respuesta correcta 3 </th>
                             <th> Puntuación obtenida</th>
                           </tr>
                         </thead>
                         <tbody>
                             <tr>
                               <td>{this.state.historialQuiz.respuestaAlmacenadaList[0].respuestaReal}</td>
                               <td>{this.state.historialQuiz.respuestaAlmacenadaList[1].respuestaReal}</td>
                               <td>{this.state.historialQuiz.respuestaAlmacenadaList[2].respuestaReal}</td>
                               <td>{this.state.suma} </td>
                             </tr>
                         </tbody>
                       </table>
                       <br/>
                   <center>  <button type="button" onClick={this.volverAlListado} className="btn btn-wd btn-success">
                    <span className="btn-label">
                    <i className="fa fa-check"></i>
                    </span>  Volver al Listado
                    </button> </center>
                     </div>
                   </div></Tab>) : (<br></br>)}

           </Tabs>
           <h5>Tiempo restante:</h5>
              <Countdown onComplete={this.disableBotones} date={this.state.dateNow + 1000*60*this.state.minutosDuracion}/>  
         </div>
         
       </div>
     </div>
   </div>
   );
    } else {
      return (
            <div className="row">
      <div className="col-md-12">
        <div className="card">
          <div className="header">
            <h3 className="title">Quiz</h3>
            <h4>  <p className="category">Lista de Quizzes </p> </h4>
          </div>

          <div className="content">

            <div className="panel-group" id="accordion">
            {this.state.cargandoQuiz ? (<center>
                                                                        <h3>Cargando...</h3><br/>
                                                                        <Loader
                                                                          type="Puff"
                                                                          color="#00BFFF"
                                                                          height="100"
                                                                          width="100"
                                                                        /></center>) : (
              <Accordion>
                {this.state.quizzes.map(function(name, index){
                        return (
                        <Panel header={<span>  {name.nombreQuiz}  <b className="caret"></b></span>} eventKey={index}>
                          <h1>  <p className="text-primary">  Fecha de apertura : </p>
                          <p className="text-warning"> {name.fechaApertura}</p>  </h1>

                          <h1>  <p className="text-primary">  Estado : </p>
                          <p className="text-warning">{name.habilitado ? 'Sin Realizar' : 'Realizado'} </p>  </h1>
                          <center> {name.habilitado ? (<button  className="btn btn-success btn-fill btn-wd" id={name.quizId} onClick={self.comenzarQuiz}> Realizar Quizz </button>) : (<button disabled className="btn btn-default  btn-fill btn-wd">No Disponible</button>)} </center>
                          
                        </Panel>)
                      })}
              </Accordion>
            )}
            </div>
          </div>
        </div>
      </div>
    </div>
          );
                    }
}

}
export default Quizzes;
