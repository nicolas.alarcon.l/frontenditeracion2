import React from 'react';
import { Accordion, Panel } from 'react-bootstrap';
import { Link } from 'react-router-dom';



const AccordionGroups = () => (
  <div className="row">
    <div className="col-md-12">
      <div className="card">
        <div className="header">
           <h3 className="title">Quiz</h3>
           <h4>  <p className="category">Lista de Quiz's </p> </h4>        
        </div>
        <div className="content">
          <div className="panel-group" id="accordion">
            <Accordion>
              <Panel header={<span>  Quiz Nº1  <b className="caret"></b></span>} eventKey="1">
                 <h1>  <p className="text-primary">  Unidades a evaluar : </p>  
                <p className="text-warning"> Unidad 1, Unidad 2 y Unidad 3 </p>  </h1>                  
                <h1>  <p className="text-primary">  Fecha de apertura : </p>  
                <p className="text-warning"> 10/02/2019 </p>  </h1>
                <h1>  <p className="text-primary">  Hora de apertura : </p>  
                <p className="text-warning"> 09:am  a 10:50 am </p>  </h1>              
                <h1>  <p className="text-primary">  Estado : </p>  
                <p className="text-warning"> Realizado </p>  </h1>         
                 <center> <button disabled className="btn btn-default  btn-fill btn-wd">No Disponible</button> </center>
              </Panel>
              <Panel header={<span>  Quiz Nº2 <b className="caret"></b></span>} eventKey="2">
               <h1>  <p className="text-primary">  Unidades a evaluar : </p>  
                <p className="text-warning"> Unidad 4, Unidad 5 y Unidad 6 </p>  </h1>                  
                <h1>  <p className="text-primary">  Fecha de apertura : </p>  
                <p className="text-warning"> 10/02/2019 </p>  </h1>
                <h1>  <p className="text-primary">  Hora de apertura : </p>  
                <p className="text-warning"> 09:am  a 10:50 am </p>  </h1>               
                <h1>  <p className="text-primary">  Estado : </p>  
                <p className="text-warning"> Sin realizar </p>  </h1>
                 <center>
                  <Link to="/components/panels">
                      <button  className="btn btn-success btn-fill btn-wd"> Realizar Quizz </button>
                  </Link>
                   </center>
              </Panel>
              <Panel header={<span>  Quiz Nº3 <b className="caret"></b></span>} eventKey="3">
               <h1>  <p className="text-primary">  Unidades a evaluar : </p>  
                <p className="text-warning"> Unidad 7, Unidad 8 y Unidad 9 </p>  </h1>                
                <h1>  <p className="text-primary">  Fecha de apertura : </p>  
                <p className="text-warning"> 10/02/2019 </p>  </h1>
                <h1>  <p className="text-primary">  Hora de apertura : </p>  
                <p className="text-warning"> 09:am  a 10:50 am </p>  </h1>
                <h1>  <p className="text-primary">  Estado : </p>  
                <p className="text-warning"> Sin realizar </p>  </h1>                
                <center> <button disabled className="btn btn-default  btn-fill btn-wd">No Disponible</button> </center>
              </Panel>
              <Panel header={<span>  Quiz Nº4 <b className="caret"></b></span>} eventKey="3">
               <h1>  <p className="text-primary">  Unidades a evaluar : </p>  
                <p className="text-warning"> Unidad 7, Unidad 8 y Unidad 9 </p>  </h1>                
                <h1>  <p className="text-primary">  Fecha de apertura : </p>  
                <p className="text-warning"> 10/02/2019 </p>  </h1>
                <h1>  <p className="text-primary">  Hora de apertura : </p>
                <p className="text-warning"> 09:am  a 10:50 am </p>  </h1>
                <h1>  <p className="text-primary">  Estado : </p>  
                <p className="text-warning"> Sin realizar </p>  </h1>                
                <center> <button disabled className="btn btn-default  btn-fill btn-wd">No Disponible</button> </center>
              </Panel>
              <Panel header={<span>  Quiz Nº5 <b className="caret"></b></span>} eventKey="3">
               <h1>  <p className="text-primary">  Unidades a evaluar : </p>  
                <p className="text-warning"> Unidad 7, Unidad 8 y Unidad 9 </p>  </h1>                
                <h1>  <p className="text-primary">  Fecha de apertura : </p>  
                <p className="text-warning"> 10/02/2019 </p>  </h1>
                <h1>  <p className="text-primary">  Hora de apertura : </p>  
                <p className="text-warning"> 09:am  a 10:50 am </p>  </h1>
                <h1>  <p className="text-primary">  Estado : </p>  
                <p className="text-warning"> Sin realizar </p>  </h1>                
                <center> <button disabled className="btn btn-default  btn-fill btn-wd">No Disponible</button> </center>
              </Panel>
              <Panel header={<span>  Quiz Nº6 <b className="caret"></b></span>} eventKey="3">
               <h1>  <p className="text-primary">  Unidades a evaluar : </p>  
                <p className="text-warning"> Unidad 7, Unidad 8 y Unidad 9 </p>  </h1>
                <h1>  <p className="text-primary">  Fecha de apertura : </p>  
                <p className="text-warning"> 10/02/2019 </p>  </h1>
                <h1>  <p className="text-primary">  Hora de apertura : </p>  
                <p className="text-warning"> 09:am  a 10:50 am </p>  </h1>                          
                <h1>  <p className="text-primary">  Estado : </p>  
                <p className="text-warning"> Sin realizar </p>  </h1>                
                <center> <button disabled className="btn btn-default  btn-fill btn-wd">No Disponible</button> </center>
              </Panel>
              <Panel header={<span>  Quiz Nº7 <b className="caret"></b></span>} eventKey="3">
               <h1>  <p className="text-primary">  Unidades a evaluar : </p>  
                <p className="text-warning"> Unidad 7, Unidad 8 y Unidad 9 </p>  </h1>                
                <h1>  <p className="text-primary">  Fecha de apertura : </p>  
                <p className="text-warning"> 10/02/2019 </p>  </h1>

                <h1>  <p className="text-primary">  Hora de apertura : </p>  
                <p className="text-warning"> 09:am  a 10:50 am </p>  </h1>
                <h1>  <p className="text-primary">  Estado : </p>  
                <p className="text-warning"> Sin realizar </p>  </h1>

                
                <center> <button disabled className="btn btn-default  btn-fill btn-wd">No Disponible</button> </center>




              </Panel>



            </Accordion>


        
          </div>




        </div>
      </div>
    </div>
  </div>
);

export default AccordionGroups;