import React from 'react';
import { Route } from 'react-router-dom';
import VectorMap from './VectorMap';

const MapsPage = ({match}) => (
  <div className="content">
    <div className="container-fluid">
      <Route path={`${match.url}/vector-map`} component={VectorMap} />
    </div>
  </div>
);

export default MapsPage;