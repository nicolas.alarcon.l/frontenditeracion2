import React from 'react';
import { Field, reduxForm } from 'redux-form';
import renderField from 'components/FormInputs/renderField';

const FormElements = () => (
 

      <form className="form-horizontal">
       


        <div className="form-group">
          <label className="control-label col-md-3">Respuesta</label>
  
          <div className="col-md-9">
            <Field
              name="placeholder"
              type="text"
              placeholder="placeholder"
              component={renderField} />
          </div>

        </div> 
      </form>
   
);

export default reduxForm({
  form: 'formElements'
})(FormElements);