import React from 'react';
import { Field, reduxForm } from 'redux-form';
import renderField from 'components/FormInputs/renderField';




const HorizontalForm = () => (
  <div className="card">
    <div className="header">
      <h4>Horizontal Form</h4>
    </div>
    <div className="content">
      <form className="form-horizontal">

        <div className="form-group">
          <label className="col-md-3 control-label">Email</label>
          <div className="col-md-9">
            <Field
              name="email"
              type="email"
              component={renderField}
              label="Email"
              />
          </div>
        </div>


       
      </form>
    </div>
  </div>
);

export default reduxForm;

