import React from 'react';
import PerformanceChart from './PerformanceChart';
const Charts = () => (
  <div className="content">
    <div className="container-fluid">
      <div className="row">
        <div className="col-md-6">
          <PerformanceChart />
        </div>
      </div>
    </div>
  </div>
);

export default Charts;