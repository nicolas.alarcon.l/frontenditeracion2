import React, { Component } from 'react';
import cx from 'classnames';
import axios from 'axios';
import ReactDOM from 'react-dom';
import { Tabs, Tab } from 'react-bootstrap';
import FormElements from './FormElements';
import { BrowserRouter as Router, Route, Link} from 'react-router-dom';
import urlBase from '../../../routes';
import '../../../styles.css';


class Tags extends Component {

   constructor(props){
    super(props);
      this.state = {


      preguntas: [],
      respuestasCorrectas: [],
      quizzes: [],
      materias: [],
      id_materia1: -1,
      id_materia2: -1,
      id_materia3: -1,
      respuesta1: " ",
      respuesta2: " ",
      respuesta3:  "",
      respCorrecta1: "",
      respCorrecta2: "",
      respCorrecta3: "",
      estadoRespuesta1: "",
      estadoRespuesta2: "",
      estadoRespuesta3: "",
      puntaje1: 0,
      puntaje2: 0,
      puntaje3: 0,
      suma:0,
      nota:1,
      historialQuiz:[],
      quizFinalizado: false,
      mounted: false,
    };
    this.handleInputChanged = this.handleInputChanged.bind(this);
    this.enviarRespuesta1 = this.enviarRespuesta1.bind(this);
    this.enviarRespuesta2 = this.enviarRespuesta2.bind(this);
    this.enviarRespuesta3 = this.enviarRespuesta3.bind(this);
    this.terminarQuiz = this.terminarQuiz.bind(this);
  }

  componentDidMount(){
    console.log(this.props);


    //fetch de todos los quizes. se almacenan en state.quizzes
    axios.get(urlBase+'/quizzes')
     .then(res => {
      //console.log(res);
       this.setState({ quizzes: res.data });
       console.log(res.data);
       axios.post(urlBase+'/historial/create',{})
       .then(res=>{
         console.log("hola X: "+ res );
         this.setState({historialQuiz: res.data});
         this.fetchPreguntas();
         this.setState({mounted: true})

             //por ahora se predefine el quiz de ejemplo con id 143.
             //la idea, mas adelante, es que se seleccione un quizz de la lista en base a los datos de cursos y usuario
      });
    });
  }

  async fetchPreguntas() {

    var historialQuizId = this.state.historialQuiz.historialId;

    var x;
    var _preguntas = [];
    var _respuestasCorrectas = [];
    console.log("Entre");
    for(x in this.state.quizzes[0].materias) {
       console.log("Entre" + x );
      var auxPregunta = "";
      var auxRespuesta = "";
      var materia = this.state.quizzes[0].materias[x];
      var materiaId = materia.materiaId;

      /*console.log('http://206.189.198.100:8081/backend-0.0.1-SNAPSHOT/app/obtener_pregunta_' + materiaId + '/' + historialQuizId);
      */

      let json = await axios.get(urlBase+'/obtener_pregunta_' + materiaId + '/' + historialQuizId);

      auxPregunta = json.data.pregunta;

      _preguntas.push(auxPregunta);
      _respuestasCorrectas.push(auxRespuesta);
      this.setState({preguntas: _preguntas});
      this.setState({respuestasCorrectas: _respuestasCorrectas});
    }

    axios.get(urlBase+'/historial/'+this.state.historialQuiz.historialId)
      .then(res => {
          console.log(res.data);
          this.setState({historialQuiz: res.data});
      });
  }


  handleInputChanged(e){
    console.log( e.target.value);
    if(e.target.name === "respuesta1"){
      console.log("respuesta1");
      this.setState({
        respuesta1: e.target.value.toString()
      });
    }else if (e.target.name === "respuesta2"){
          //console.log("respuesta2");
      this.setState({
        respuesta2: e.target.value.toString()
      });
    }else if (e.target.name === "respuesta3"){
      this.setState({
        respuesta3: e.target.value.toString()
      });
    }
  }

  enviarRespuesta1() {
    var estado = {
        respuestaAlumno: this.state.respuesta1,
        numeroRespuesta: 1
    };
    var respuestaId = this.state.historialQuiz.respuestaAlmacenadaList[0].respuestaAlmacenadaId;
    console.log(respuestaId);
    axios.put(urlBase+'/respuestas/update/'+respuestaId, estado)
    .then(res => {
        console.log(res);
        alert('Respuesta 1 enviada')})
        .catch(error =>{
            alert(error)
    })
  }

  enviarRespuesta2() {
    var estado = {
        respuestaAlumno: this.state.respuesta2,
        numeroRespuesta: 2
    };
    var respuestaId = this.state.historialQuiz.respuestaAlmacenadaList[1].respuestaAlmacenadaId;
    console.log(respuestaId);
    axios.put(urlBase+'/respuestas/update/'+respuestaId, estado)
        .then(res => {
            console.log(res);
            alert('Respuesta 2 enviada')})
            .catch(error =>{
                alert(error)
        })
  }

  enviarRespuesta3() {
      var estado = {
          respuestaAlumno: this.state.respuesta3,
          numeroRespuesta: 3
      };
      var respuestaId = this.state.historialQuiz.respuestaAlmacenadaList[2].respuestaAlmacenadaId;
      console.log(respuestaId);
      axios.put(urlBase+'/respuestas/update/'+respuestaId, estado)
          .then(res => {
              console.log(res);
              alert('Respuesta 3 enviada')})
              .catch(error =>{
                  alert(error)
          })
  }

  terminarQuiz(){
    axios.get(urlBase+'/historial/'+this.state.historialQuiz.historialId)
    .then(res => {
      console.log(res);
      var historial = res.data;
      this.setState({historialQuiz: res.data});

      var sumaAux = historial.respuestaAlmacenadaList[0].putajeRespuesta+historial.respuestaAlmacenadaList[1].putajeRespuesta+historial.respuestaAlmacenadaList[2].putajeRespuesta;
      var notaAux = 1+sumaAux;
      console.log(sumaAux);
      console.log(notaAux);
      this.setState({suma: sumaAux});
      this.setState({nota: notaAux});
      alert('Quiz entregado')})
      .catch(error =>{
          alert(error)
    }).finally(this.setState({quizFinalizado: true}));
  }


  render() {
    return (
       <div className="row">
    <div className="col-md-12">
      <div className="card">
        <div className="header" >
          <h4 className="title">Quiz Nº1</h4>

        </div>
        <div className="content">
          <Tabs defaultActiveKey={1} id="plan-text-tabs">
            <Tab eventKey={1} title="Pregunta Nº 1">

             <h6> PREGUNTA  Nº1 </h6>


             <h6> {this.state.preguntas[0]} </h6>


                  <div className="col-md-12">
                      <input type="text" name="respuesta1" onChange={this.handleInputChanged} />
                  </div>

                 <center>  <button type="button" onClick={this.enviarRespuesta1} className="btn btn-wd btn-success">

                  <span className="btn-label">
                  <i className="fa fa-check"></i>
                  </span>  Enviar
                  </button> </center>

            </Tab>
            <Tab eventKey={2} title="Pregunta Nº 2">

                <h6> PREGUNTA  Nº2 </h6>
             <h6>  {this.state.preguntas[1]}</h6>


                  <div className="col-md-12">
                      <input type="text" name="respuesta2" onChange={this.handleInputChanged}/>
                  </div>


                 <center>  <button type="button" onClick={this.enviarRespuesta2} className="btn btn-wd btn-success">
                  <span className="btn-label">
                  <i className="fa fa-check"></i>
                  </span>  Enviar
                  </button> </center>


            </Tab>
            <Tab eventKey={3} title="Pregunta Nº 3">
                  <h6> PREGUNTA  Nº3 </h6>
             <h6>  {this.state.preguntas[2] } </h6>


                  <div className="col-md-12">
                       <input type="text" name="respuesta3" onChange={this.handleInputChanged}/>
                  </div>


                 <center>  <button type="button" onClick={this.enviarRespuesta3} className="btn btn-wd btn-success">
                  <span className="btn-label">
                  <i className="fa fa-check"></i>
                  </span>  Enviar
                  </button> </center>

                  <br/>
                  <center>  <button type="button" onClick={this.terminarQuiz} className="btn btn-wd btn-success">
                   <span className="btn-label">
                   <i className="fa fa-check"></i>
                   </span>  Terminar Quiz
                   </button> </center>







            </Tab>
            {this.state.quizFinalizado ? (<Tab eventKey={4} title="Resultados quiz">
                  <h6> RESULTADOS </h6>
                  <div className="card">
                    <div className="header">
                      <h4 className="title">Información quiz rendido </h4>
                      <p className="category">Detalles</p>
                    </div>
                    <div className="content table-responsive table-full-width">
                      <table className="table table-hover table-striped">
                        <thead>
                          <tr>
                            <th> Puntaje respuesta 1 </th>
                            <th> Puntaje respuesta 2 </th>
                            <th> Puntaje respuesta 3 </th>
                            <th> Nota obtenida </th>
                          </tr>
                        </thead>
                        <tbody>
                            <tr>
                              <td>{this.state.historialQuiz.respuestaAlmacenadaList[0].putajeRespuesta}</td>
                              <td>{this.state.historialQuiz.respuestaAlmacenadaList[1].putajeRespuesta}</td>
                              <td>{this.state.historialQuiz.respuestaAlmacenadaList[2].putajeRespuesta}</td>
                              <td> {this.state.nota}</td>
                            </tr>
                        </tbody>
                        <thead>
                          <tr>
                            <th> Respuesta correcta 1 </th>
                            <th> Respuesta correcta 2 </th>
                            <th> Respuesta correcta 3 </th>
                            <th> Puntuación obtenida</th>
                          </tr>
                        </thead>
                        <tbody>
                            <tr>
                              <td>{this.state.historialQuiz.respuestaAlmacenadaList[0].respuestaReal}</td>
                              <td>{this.state.historialQuiz.respuestaAlmacenadaList[1].respuestaReal}</td>
                              <td>{this.state.historialQuiz.respuestaAlmacenadaList[2].respuestaReal}</td>
                              <td>{this.state.suma} </td>
                            </tr>

                        </tbody>
                      </table>

                    </div>
                  </div></Tab>) : (<br></br>)}


          </Tabs>
        </div>
      </div>
    </div>
  </div>
  );
}

}

export default Tags;
