import React from 'react';
import Tags from './Tags';

const Panels = () => (
  <div className="content">
    <div className="container-fluid">
      <Tags />
    </div>
  </div>
);
export default Panels;