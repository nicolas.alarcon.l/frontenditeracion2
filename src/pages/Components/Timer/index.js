import React from 'react';
import Temporizador from './Timer';

const Timer = () => (
  <div className="content">
    <div className="container-fluid">
      <Temporizador />
    </div>
  </div>
);
export default Timer;