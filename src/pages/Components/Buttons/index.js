import React  from 'react';
import { render } from 'react-dom';
import brace from 'brace';
import AceEditor from 'react-ace';
import AccordionGroups from './AccordionGroup';
import AccordionGroups2 from './AccordionGroup2';
import NewPregunta from './NewPregunta';
import NewQuiz from './NewQuiz';
import NewPozo from './NewPozo';


import 'brace/mode/python';
import 'brace/theme/github';


 function onChange(newValue) {
   console.log('change',newValue);
  };

const ButtonsPage = () => (

  <div className="content">
    <div className="container-fluid">
      <div className="row">
      </div>
      <div className="col-md-12">
        <NewPozo />
      </div>
      <div className="col-md-12">
        <NewPregunta />
      </div>
      <div className="col-md-12">
        <NewQuiz />
      </div>
    </div>
  </div>






);

export default ButtonsPage;
