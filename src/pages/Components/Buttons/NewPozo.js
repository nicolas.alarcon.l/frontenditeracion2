
import React, { Component } from 'react';
import cx from 'classnames';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { Accordion, Panel,FormGroup,ControlLabel,FormControl } from 'react-bootstrap';
import AceEditor from 'react-ace';
import urlBase from '../../../routes';
class NewPozo extends Component {

	 constructor(props){
    super(props);

      this.state =
      {
        "nombreMateria": ""
      }

      this.handleInputChanged = this.handleInputChanged.bind(this);
      this.create = this.create.bind(this);
      this.onChange = this.onChange.bind(this);
  	}

   onChange(newValue) {
        this.setState({nombreMateria: newValue});
    }

    handleInputChanged(e)
    {

      console.log( e.target.value);
      this.setState({
        nombreMateria: e.target.value
      });
    }

 componentDidMount(){

  }

  create() {

  axios.post(urlBase+'/crearMateria', this.state)
    .then(res => {
      alert('Pozo Ingresado')})
    .catch(error =>{
        alert(error)
    })
}

  render() {
    return (
			<div className="row">
		    <div className="col-md-12">
		      <div className="card">
		        <div className="header">
		           <h3 className="title">Nuevo Pozo</h3>
		           <h4>  <p className="category">Agregar nuevo pozo</p> </h4>
		        </div>
		        <div className="content">
		          <div className="panel-group" id="accordion">
								<center>
									<div className="col-md-12">
			           		<h5> Ingrese nombre del pozo </h5>
			           		<input type="text" name="nombreMateria"  onChange={this.handleInputChanged}/>
                     <br/><br/>
		        			</div>  
            			<button   className="btn btn-default btn-fill btn-wd" onClick= {this.create} >  Crear </button>
								</center>
          		</div>
        		</div>
      		</div>
    		</div>
  		</div>
    );
	}
}
export default NewPozo;
