import React from 'react';
import { Accordion, Checkbox,Panel,FormGroup,ControlLabel,FormControl } from 'react-bootstrap';

const AccordionGroups2 = () => (
  <div className="row">
    <div className="col-md-12">
      <div className="card">
        <div className="header">
           <h3 className="title">Nuevo Quiz</h3>
           <h4>  <p className="category">Agregar nuevo Quiz</p> </h4>
        </div>
        <div className="content">
          <div className="panel-group" id="accordion">
            <Accordion>
              <Panel header={<span> Materias <b className="caret"></b></span>} eventKey="1">     
                  <FormGroup>
                  <Checkbox inline>Unidad 1</Checkbox> 
                  <Checkbox inline>Unidad 2</Checkbox>{' '}
                  <Checkbox inline>Unidad 3</Checkbox>
                  <Checkbox inline>Unidad 4</Checkbox>
                  <Checkbox inline>Unidad 5</Checkbox>
                  <Checkbox inline>Unidad 6</Checkbox>
                  <Checkbox inline>Unidad 7</Checkbox>
                  </FormGroup>
              </Panel>
              <Panel header={<span> Fecha <b className="caret"></b></span>} eventKey="2">   
              </Panel>
              <center> <button disabled className="btn btn-default  btn-fill btn-wd">Crear</button> </center>
            </Accordion>        
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default AccordionGroups2;