
import React, { Component } from 'react';
import cx from 'classnames';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { Accordion, Panel,FormGroup,ControlLabel,FormControl } from 'react-bootstrap';
import AceEditor from 'react-ace';
import urlBase from '../../../routes';
import 'brace/theme/eclipse';
import Tests from './Tests';
class NewPregunta extends Component {

	 constructor(props){
    super(props);
      this.state = {
        "estado": true,
        "materiaId": 1,
        "pregunta": "",
        "preguntaId": 0,
				"materias": [],
				"test":[]
			}
    this.handleChange = this.handleChange.bind(this);

        this.create = this.create.bind(this);
          this.onChange = this.onChange.bind(this);
  }

   handleChange(event) {
    this.setState({materiaId: event.target.value});

  }

   onChange(newValue) {
        this.setState({pregunta: newValue});
    }

 componentDidMount(){
  //fetch materias
  axios.get(urlBase+'/materias')
    .then(res => {
      this.setState({materias: res.data});
			console.log(this.state.materias);
			console.log(this.state.test);
		})
		
  }	

	async create() {
		//se recorre el arreglo de campos de variables
		console.log("entre0");
		var x;
		var tuplas = this.state.test.state.tuples;
		console.log(tuplas);
		
  	var enviarPregunta = {
      "estado": this.state.estado,
      "materiaId": this.state.materiaId,
      "pregunta": this.state.pregunta
    }
    let postPregunta = await axios.post(urlBase+'/crear_pregunta', enviarPregunta)
		var preguntaId = postPregunta.data.preguntaId;

			for(x in tuplas) {
				var enviarVariable = {
					"nombreVariable": tuplas[x].current.state.in,
					"preguntaId": preguntaId
				}
				let postVariable = await axios.post(urlBase+'/variable/create', enviarVariable);
			}
			alert("Pregunta ingresada");
  }

  render() {
    return (
			<div className="row">
		    <div className="col-md-12">
		      <div className="card">
		        <div className="header">
		           <h3 className="title">Nueva Pregunta</h3>
		           <h4>  <p className="category">Agregar nueva pregunta</p> </h4>
		        </div>


		        <Tests ref={(test) => {this.state.test = test;}}/>


		        <div className="content">
		          <div className="panel-group" id="accordion">
							<center>
			              <h5> Ingrese código</h5>
			                <AceEditor
			                      mode="python"
			                      theme="eclipse"
			                      name="blah2"
			                      disabled
			                      onLoad={this.onLoad}
			                      onChange={this.onChange}
			                      fontSize={14}
			                      showPrintMargin={true}
			                      showGutter={true}
			                      highlightActiveLine={true}
			                      value={this.state.pregunta}
			                      setOptions={{
			                      enableBasicAutocompletion: false,
			                      enableLiveAutocompletion: false,
			                      enableSnippets: false,
			                      showLineNumbers: true,
			                      tabSize: 2,
			                			}}/>

										<h5><p className="category">Seleccione un pozo para guardar la pregunta</p> </h5>
		                <select value={this.state.materiaId} onChange={this.handleChange}>
		                  {this.state.materias.map(function(name, index){
		                    return <option value={name.materiaId}>{name.nombreMateria}</option>
		                  })}
										</select>
								</center>
		            <center>
									<br/>
		            	<button   className="btn btn-default btn-fill btn-wd" onClick= {this.create} >  Guardar </button>
								</center>
							</div>
		        </div>
		      </div>
		    </div>
		  </div>
    );
	}
}
export default NewPregunta;
