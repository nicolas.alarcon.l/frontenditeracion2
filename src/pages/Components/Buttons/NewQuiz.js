import React , {Component} from 'react';
import { Accordion, Checkbox,Panel,FormGroup,ControlLabel,FormControl } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { render } from 'react-dom';
import brace from 'brace';
import axios from 'axios';
import urlBase from '../../../routes';





class NewQuiz extends Component {

	 constructor(props){
    super(props);
      this.state = {

        unidad1 : "1",
        unidad2 : "2",
        unidad3 : "3",
				puntaje1 : "3",
				puntaje2 : "2",
        puntaje3 : "2",
        minutosDuracion:"0",

        fechaApertura:"1996-12-12",
        año : "ano",
        mes : "mes",
        dia : "dia",
        materias: [],

        habilitado: true,
				nombreQuiz: "",
        numeroQuiz: 0
    	}
     this.create = this.create.bind(this);
     this.handleChangeUnidad1 = this.handleChangeUnidad1.bind(this);
     this.handleChangeUnidad2 = this.handleChangeUnidad2.bind(this);
     this.handleChangeUnidad3 = this.handleChangeUnidad3.bind(this);
     this.handleChangePuntaje1 = this.handleChangePuntaje1.bind(this);
     this.handleChangePuntaje2 = this.handleChangePuntaje2.bind(this);
     this.handleChangePuntaje3 = this.handleChangePuntaje3.bind(this);
     this.inputEventano = this.inputEventano.bind(this);
     this.inputEventmes = this.inputEventmes.bind(this);
     this.inputEventdia = this.inputEventdia.bind(this);
		 this.inputEventDuracion = this.inputEventDuracion.bind(this);
     this.handleInputChanged = this.handleInputChanged.bind(this);

  }

 	handleChangeUnidad1(event) {
    this.setState({
      unidad1: event.target.value
    });
	}

  handleChangeUnidad2(event) {
    this.setState({
    	unidad2: event.target.value
		});
  }

  handleChangeUnidad3(event) {
    this.setState({
			unidad3: event.target.value
		});
  }

  handleChangePuntaje1(event) {
    this.setState({
      puntaje1: event.target.value
    });
  }

  handleChangePuntaje2(event ) {
    this.setState({
      puntaje2: event.target.value
    });
  }

  handleChangePuntaje3(event) {
    this.setState({
      puntaje3: event.target.value
    });
  }

	inputEventano(event){
    this.setState({
			ano: event.target.value
		});
	}


	inputEventmes(event){
	    this.setState({
				mes: event.target.value
			});
	}

	inputEventdia(event){
	    this.setState({
				dia: event.target.value
			});
	}

	handleInputChanged(e)
	{
		console.log( e.target.value);
		this.setState({
			nombreQuiz: e.target.value
		});
	}

	inputEventDuracion(event){
	    this.setState({
				minutosDuracion: event.target.value
			});
	}

	componentDidMount() {
	  //fetch materias
	  axios.get(urlBase+'/materias')
	    .then(res => {
				this.setState({materias: res.data});
				console.log(res.data);	
				//los siguientes set state son para que se consideren bien los campos por defecto en la seleccion de pozos
				this.setState({unidad1: res.data[0].materiaId});
				this.setState({unidad2: res.data[1].materiaId});
				this.setState({unidad3: res.data[2].materiaId});
    	})
	}

    create() {
      var enviar = {
          fechaApertura: this.state.ano+'-'+this.state.mes+'-'+this.state.dia,
          habilitado : this.state.habilitado,
          numeroQuiz : this.state.numeroQuiz,
          minutosDuracion: this.state.minutosDuracion,
          nombreQuiz: this.state.nombreQuiz
      };

		axios.post(urlBase+'/crear_quizz',enviar )
      .then(res => {
					axios.post(urlBase+'/quizz/'+res.data.quizId+'/materia/'+this.state.unidad1, {puntaje: this.state.puntaje1})
					.then(rres => {
						axios.post(urlBase+'/quizz/'+res.data.quizId+'/materia/'+this.state.unidad2, {puntaje: this.state.puntaje2})
						.then(rres => {
							axios.post(urlBase+'/quizz/'+res.data.quizId+'/materia/'+this.state.unidad3, {puntaje: this.state.puntaje3})
						})
					})
      }).finally(alert("quiz ingresado"));
  }

  render() {
    return (

			<div className="row">
			  <div className="col-md-12">
			    <div className="card">
			      <div className="header">
			         <h3 className="title">Nuevo Quiz</h3>
			         <h4> <p className="category">Agregar nuevo Quiz</p> </h4>
			      </div>
						<center>
				      <table className="table">
								<thead className = "thead-light">
									<tr>
										<th scope = "col">Nombre del Quiz</th>
										<th scope = "col">
											<input type="text" name="nombreQuiz"  onChange={this.handleInputChanged}/>
										</th>
									</tr>
								</thead>
							</table>
						</center>
						<center>
							<table className="table">
								<thead className = "thead-light">
									<tr>
										<th scope = "col">Pregunta</th>
										<th scope = "col">Pozo</th>
										<th scope = "col">Puntaje</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th scope = "row">Pregunta 1</th>
				      			<td>
				              <select value={this.state.unidad1} onChange={this.handleChangeUnidad1}>
				                {this.state.materias.map(function(name, index){
				                  return <option value={name.materiaId}>{name.nombreMateria}</option>
				                })}
				              </select>
										</td>
										<td>
											<input type="text" name="puntaje1"  onChange = {this.handleChangePuntaje1}/>
										</td>
									</tr>
									<tr>
										<th scope = "row">Pregunta 2</th>
				      			<td>
											<select value={this.state.unidad2} onChange={this.handleChangeUnidad2}>
												{this.state.materias.map(function(name, index){
													return <option value={name.materiaId}>{name.nombreMateria}</option>
												})}
											</select>
										</td>
										<td>
											<input type="text" name="puntaje2"  onChange = {this.handleChangePuntaje2}/>
										</td>
									</tr>
									<tr>
										<th scope = "row">Pregunta 3</th>
				      			<td>
											<select value={this.state.unidad3} onChange={this.handleChangeUnidad3}>
											{this.state.materias.map(function(name, index){
												return <option value={name.materiaId}>{name.nombreMateria}</option>
											})}
											</select>
										</td>
										<td>
											<input type="text" name="puntaje3"  onChange = {this.handleChangePuntaje3}/>
										</td>
									</tr>
								</tbody>
							</table>
							<h5>  Tiempo de Duración (minutos) </h5>
							<input type="text"  onChange={this.inputEventDuracion} name="duracion" placeholder="Tiempo"/><br/>
			        <br/><h5>  Fecha de habilitación </h5>
		          <input type="text"  onChange={this.inputEventano} name="respuesta1" placeholder="año "/>  <label> - </label>
		          <input type="text" onChange={this.inputEventmes} name="respuesta1" placeholder="mes " />  <label> - </label>
		          <input type="text" onChange={this.inputEventdia} name="respuesta1"  placeholder="día "/>
						</center>
			      <center>
							<br/>
							<button  className="btn btn-default  btn-fill btn-wd" onClick= {this.create}  >Crear</button>
							<br/>
						</center>
			  	</div>
					<br/>
				</div>
			</div>
		);
	}
}
export default NewQuiz;
