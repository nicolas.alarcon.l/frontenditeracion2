import React from 'react';
import { Accordion, Panel,FormGroup,ControlLabel,FormControl } from 'react-bootstrap';
import AceEditor from 'react-ace';

const AccordionGroups = () => (
  <div className="row">
    <div className="col-md-12">
      <div className="card">
        <div className="header">
           <h3 className="title">Nueva Pregunta</h3>
           <h4>  <p className="category">Agregar nueva pregunta</p> </h4>
        </div>
        <div className="content">
          <div className="panel-group" id="accordion">
                  <FormGroup controlId="formControlsSelect">
      <ControlLabel>Materia</ControlLabel>
      <FormControl componentClass="select" placeholder="select">
        <option value="select">Unidad 1 </option>
        <option value="other">Unidad 2</option>
      </FormControl>
    </FormGroup>
            <Accordion>
              <Panel header={<span> CODIGO <b className="caret"></b></span>} eventKey="1">     
                <AceEditor
                      mode="java"
                      theme="github"
                      name="UNIQUE_ID_OF_DIV"
                      editorProps={{$blockScrolling: true}}
                 />
              </Panel>
                 <center> <button disabled className="btn btn-default  btn-fill btn-wd">Crear</button> </center>
            </Accordion>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default AccordionGroups;