import React from 'react';
import { Tabs, Tab } from 'react-bootstrap';
import FormElements from './FormElements';



const TabGroup = () => (
  <div className="row">
    <div className="col-md-12">
      <div className="card">
        <div className="header">
          <h4 className="title">Quiz Nº1</h4>
        </div>
        <div className="content">
          <Tabs defaultActiveKey={1} id="plan-text-tabs">
            <Tab eventKey={1} title="Pregunta Nº 1">
             <h6> PREGUNTA  Nº1 </h6>
             <h6> VER FOMRA DE VISUALIZAR COMO IMAGEN </h6>
                  <div className="col-md-12">
                      <FormElements initialValues={{
                        radioGroup: 'male',
                        a: true,
                        checked: true,
                        disabledChecked: true,
                        radioOnOff: 'on',
                        radioDisabledOnOff: 'on'
                      }} />
                  </div>
                 <center>  <button type="button" className="btn btn-wd btn-success">
                  <span className="btn-label">
                  <i className="fa fa-check"></i>
                  </span>  Enviar 
                  </button> </center>
            </Tab>
            <Tab eventKey={2} title="Pregunta Nº 2">
                <h6> PREGUNTA  Nº2 </h6>
             <h6> VER FOMRA DE VISUALIZAR COMO IMAGEN </h6>
                  <div className="col-md-12">
                      <FormElements initialValues={{
                        radioGroup: 'male',
                        a: true,
                        checked: true,
                        disabledChecked: true,
                        radioOnOff: 'on',
                        radioDisabledOnOff: 'on'
                      }} />
                  </div>
                 <center>  <button type="button" className="btn btn-wd btn-success">
                  <span className="btn-label">
                  <i className="fa fa-check"></i>
                  </span>  Enviar 
                  </button> </center>
            </Tab>
            <Tab eventKey={3} title="Pregunta Nº 3">
                  <h6> PREGUNTA  Nº3 </h6>
             <h6> VER FOMRA DE VISUALIZAR COMO IMAGEN </h6>
                  <div className="col-md-12">
                      <FormElements initialValues={{
                        radioGroup: 'male',
                        a: true,
                        checked: true,
                        disabledChecked: true,
                        radioOnOff: 'on',
                        radioDisabledOnOff: 'on'
                      }} />
                  </div>
                 <center>  <button type="button" className="btn btn-wd btn-success">
                  <span className="btn-label">
                  <i className="fa fa-check"></i>
                  </span>  Enviar 
                  </button> </center>
            </Tab>
          </Tabs>
        </div>
      </div>
    </div>
  </div>
);

export default TabGroup;