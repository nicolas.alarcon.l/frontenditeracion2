import React from 'react';
import Tags from './Tags';

const Historial = () => (
  <div className="content">
    <div className="container-fluid">
      <Tags />
    </div>
  </div>
);

export default Historial;