import React from 'react';
import { Field, reduxForm } from 'redux-form';
import renderField from 'components/FormInputs/renderField';

const FormElements = () => (
      <form className="form-horizontal">
        <div className="form-group">
        <div className="col-md-6">
          <label className="control-label col-md-3">Respuesta</label>     
          <div className="col-md-9">
            <Field
              name="placeholder"
              type="text"
              placeholder="Escribr Respuesta"
              disabled = {true}
              component={renderField} />
          </div>
        </div> 
          <div className="col-md-6">
          <label className="control-label col-md-6">Tu Respuesta</label>
          <div className="col-md-6">
            <Field
              name="placeholder"
              type="text"
              placeholder="Escribr Respuesta"
              disabled = {true}
              component={renderField} />
          </div>
            </div>
        </div> 
   </form>
);

export default reduxForm({
  form: 'formElements'
})(FormElements);