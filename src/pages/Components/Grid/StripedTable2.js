import React from 'react';
import { Link } from 'react-router-dom';


const StripedTable2 = ({data}) => (
  <div className="card">
    <div className="header">
      <h4 className="title">Preguntas Pendientes </h4>
      <p className="category">Lista de preguntas pendientes</p>
    </div>
    <div className="content table-responsive table-full-width">
      <table className="table table-hover table-striped">
        <thead>
          <tr>
            <th> ID </th>
            <th> Materias </th>
            <th> Profesor </th>
            <th> Ver Pregunta </th>
              <th> Acción</th>

          </tr>
        </thead>
        <tbody>
          {data.map(item => (
            <tr key={item.id}>
              <td>{item.id}</td>
              <td> 1 ,2 y 3 </td>
              <td> Luis paredes</td>
            
              <td> 

                 <Link to="/components/grid">
                          <button type="button" className="btn btn-wd btn-warning">
                <span className="btn-label">
                </span> Ver Detalles 
              </button>

               </Link>


           </td>
           <td> 
              Aceptar /Rechazar 
           </td>
            </tr>
          ))}
        </tbody>
      </table>

    </div>
  </div>
);

export default StripedTable2;