import React , {Component} from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import urlBase from '../../../routes';



class Preguntas extends Component {

	 constructor(props){
    super(props);
      this.state = {

        preguntas : []

  
    }
  
  }

 componentDidMount(){

  axios.get(urlBase+'/preguntas')
      .then(res => {
          
          console.log(res);
          this.setState({ preguntas: res.data });
      }) 

  }

  

  render() {
    return (
              <div className="card">
    <div className="header">
      <h4 className="title">Preguntas </h4>
      <p className="category">Lista de preguntas</p>
    </div>
    <div className="content table-responsive table-full-width">
      <table className="table table-hover table-striped">
        <thead>
          <tr>
            <th> </th>
            <th> Materias </th>
            <th> Profesor </th>
            <th> Status </th>
             <th> Ver Pregunta </th>
          </tr>
        </thead>
        <tbody>


            <tr>
              <td> </td>
              <td> 1 ,2 y 3 </td>
              <td> Luis paredes</td>
              <td>  Aprobada </td>
              <td> 

                 <Link to="/components/grid">
                          <button type="button" className="btn btn-wd btn-warning">
                <span className="btn-label">
                </span> Ver Detalles 
              </button>

               </Link>


           </td>
            </tr>
        


        </tbody>
      </table>

    </div>
  </div>

    	  );
}

}
export default Preguntas;