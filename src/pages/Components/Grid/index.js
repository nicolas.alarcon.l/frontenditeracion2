import React from 'react';
import Preguntas from './Preguntas';
import StripedTable2 from './StripedTable2';
import generateData from './generateData';

const data = generateData(5);
const data2 = generateData(5);

const Grid = () => (

<div className="content">
    <div className="container-fluid">
      <div className="row">
        
        <div className="col-md-12">
        		  <Preguntas />
        </div>
        <div className="col-md-12">
           <StripedTable2 data={data2} />
        </div> 
      </div>
    </div>
  </div>
);

export default Grid;